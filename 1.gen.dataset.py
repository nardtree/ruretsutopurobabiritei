import os
import glob
import math

import sys

BLACK = set([2,4,6,8,10,11,13,15,17,20,22,24,26,28,29,31,33,35])
RED   = set([1,3,5,7,9,12,14,16,18,19,21,23,25,27,30,32,34,36])
GREEN = set([0])

tape = []
class D(object):
  def __init__(self):
    self.Red   = 0
    self.Black = 0
    self.Green = 0
for name in glob.glob("*dataset.txt"):
  with open(name, 'r') as f:
    next(f)
    next(f)
    next(f)

    for line in f:
      try:
        line =int(line.strip())
      except:
        continue
      if line in BLACK:
        tape.append("B")
      if line in RED:
        tape.append("R")
      if line in GREEN:
        tape.append("G")
  WINDOW = [2,3,4,5,6,7]

  win_next = {}
  for w in WINDOW:
    for i in range(len(tape) - w):
      win  = "".join(tape[i:i+w])
      nxt  = tape[i+w]
      #print(win, nxt)
      if win_next.get(win) is None:
        win_next[win] = D()

      if nxt == "B":
        win_next[win].Black += 1
      if nxt == "G":
        win_next[win].Green += 1
      if nxt == "R":
        win_next[win].Red   += 1

  keys = sum( [ list(map(lambda x:y*x, WINDOW)) for y in ["R", "B", "G"] ], [] )

  for k in keys:
    for y,f,p in ("R", lambda x:x.Red, 0.486486486486486), \
                ("B", lambda x:x.Black, 0.486486486486486),\
                ("G",lambda x:x.Green, 0.027027027027027):
      try:
        d     = win_next[k]
      except KeyError as e:
        print(e)
        continue
      total = d.Red + d.Black + d.Green
      if k[-1] == y:
        print(name, k, y,f(d)/total, p, p > f(d)/total, total)

